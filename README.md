CtrlV Template Manager v0.05a

A small python program for handling email templates for fast transfer to clipboard (currently xsel is used). 

This is my first python project, so the code is definitely not perfect.

TODO:

    1. create a proper command line arguments reader (eg. specifying custom config file, etc)
    2. write proper config file reader to override defaults set in main.py (eg. ~/.ctrlv/ctrlv.conf)
    3. write a CLI tool to manage the database (including importing FlashPaste database, etc)
    4. FIXME - need to check if terminal support colors and adjust accordingly. 
       Currently - terminals that do not support colors are not supported. CtrlV is crashing. 
    5. FIXME - after editing a node text (by running an external program), cursor is no longer hidden, 
       although curses.curs_set its set to 0
    6. FEATURE: create a proper search functionality to search through all templates and highlight 
       found strings.
       
Changes from v0.05

    1. cleaned up/rewritten some code. removed many reduntant properties.
    2. ui.py: removed self.__data and therefore ambiguity between self.__data and self.data
       a. now self.data is the data storage of the current branch in the tree. Always.
       b. there is a new property self.root_data pointing to the root of the tree to be used by "save"
    3. created more informative status bar showing the level map like example below:
       root level >> child1 >> child1-1

Changes from v0.04a:

    1. removed the most annoying screen flickering. Now its nice and solid. 
    2. minor optimizations

Changes from v0.04:

    1. cosmetic change: switched to xsel and now copying to all 3 clipboards (PRIMARY, SECONDARY, CLIPBOARD)

Changes from v0.03

    1. cosmetic change: now xclip is set to use XA_CLIPBOARD, not XA_PRIMARY. 

Changes from v0.02

    1. FEATURE: added new shortcut to copy signature only (g or F12 by default).


Changes from v0.01

    1. CHANGE: Default db file location is changed: now all ctrlv files are stored in ~/.ctrlv directory
    2. BUGFIX: ctrlv was crashing on terminal resize. Now on resize, it does not crash and NOT render 
       properly until next key press. I think its acceptable and is not worth of changing the code 
       to fix that.
    3. FEATURE: new shortcuts were included ('Y' or F4 by default) that append the contents of .signature 
       file in the config directory. Useful for email templates to contain default signature as well.
    4. Minor optimization, code cleanup and bug fixes. 

