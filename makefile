run:
	@python2 ctrlv 2>debug.out

install:
	python2 setup.py build
	python2 setup.py install --record install.log

uninstall:
	@echo using install.log to uninstall the program
	@cat install.log | xargs rm -f

dist:
	python setup.py sdist

clean:
	-rm -f *.pyc
	-rm -f libctrlv/*.pyc
	-rm -rf bin
	-rm -rf dist
	-rm -rf build
	-rm -f debug.out
	-rm -f MANIFEST

