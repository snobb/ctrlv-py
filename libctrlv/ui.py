#!/usr/bin/env python2
#
# { ui.py }
# Copyright (C) 2012-2013 Alex Kozadaev [akozadaev at yahoo com]
#

import libctrlv as ctrlv
import os, sys, abc, tempfile
import curses as c
import curses.textpad

# TODO: REFACTOR!!! REFACTOR!!! REFACTOR!!!

class View(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def attach_controller(self, controller): pass

    @abc.abstractmethod
    def mainloop(self): pass

    @abc.abstractmethod
    def save(self): pass

class AbstractController(object):
    __metaclass__ = abc.ABCMeta

    @property
    def data(self): pass

    @data.setter
    def data(self, data): pass

    @property
    def model(self): pass

    @abc.abstractmethod
    def get_clipboard(self): pass

    @abc.abstractmethod
    def set_clipboard(self): pass

    @abc.abstractmethod
    def write(self): pass

    @abc.abstractmethod
    def run(self): pass

class CursesView(View):
    """ View class providing Curses based UI for CtrlV """
    def __init__(self, editor="vim"):
        """ initialize class variables and curses """
        self.__controller = None
        self.data = None
        self.__editor = editor
        self.screen = c.initscr()

        self._setup_curses() # set curses parameters

        self.h, self.w = self.screen.getmaxyx() # screem dymensions

        self._tpos   = 0                      # tpos - top upper visible
        self._tlines = 19                     # tlines - number of lines in top window
        self._bpos   = 0                      # bpos - bottom upper visible
        self._btop   = self._tlines+2         # btop - start(y) of bottom window.
        self._blines = self.h-self._tlines-3  # blines - number of lines in bottom window
        self._cursor = 0                      # cursor - cursor position
        self._dirty  = -1                     # if dirty == 0 - clear statusbar. -1 - dont count
        self._bpercent = 0                    # current position in the bottom windows (percente)

        self.topwin = c.newwin(self._tlines+1, self.w, 1, 0)           # top window
        self.bottomwin = c.newwin(self._blines, self.w, self._btop, 0) # bottom window

        self._levbuf = []  # data, tpos and _cursor stack for lower levels
        c.start_color() # FIXME - need to check if terminal support colors and adjust accordingly 

        c.init_pair(1, c.COLOR_WHITE, c.COLOR_BLUE)
        c.init_pair(2, c.COLOR_BLUE, c.COLOR_YELLOW)
        c.init_pair(3, c.COLOR_YELLOW, c.COLOR_BLUE)
        c.init_pair(4, c.COLOR_RED, c.COLOR_BLACK)

        self.topwin.bkgdset(c.color_pair(3))

    def __del__(self):
        """ cleanup curses on object destruction """
        self.cleanup()

    # ==[ properties ]=========================================================
    @property
    def root_data(self):
        """ get the top level data """
        if len(self._levbuf) > 0:
            return self._levbuf[0][0]
        else:
            return self.data

    @property
    def dpos(self):
        """ current position in the main data structure self.data """
        return self._cursor + self._tpos

    @property
    def indicator(self):
        """ return a formated indicator string """
        return "L%d %3d%%" % (len(self._levbuf), self._bpercent)

    # ==[ internal(private) functions ]========================================
    def _setup_curses(self):
        """ set curses initial configuration """
        c.noecho()
        c.cbreak()
        c.curs_set(0)
        self.screen.keypad(1)

    def _get_long_line(self, msg, ch=" "):
        """ get a string with ch padding of the width of the screen """
        return "%s%s" % (msg, ch*(self.w-len(msg)))

    def _get_user_input(self, prompt=":", value=None):
        """ get user input in the statusbar """
        c.curs_set(1)
        self.screen.addstr(self.h-1, 0, prompt)
        self.screen.refresh()
        inputwin = c.newwin(1, self.w, self.h-1, len(prompt))
        tbox = curses.textpad.Textbox(inputwin, insert_mode=True)
        if (value != None):
            inputwin.addstr(0, 0, value)
        text = tbox.edit().strip()
        self.screen.hline(self.h-1, 0, " ", self.w)
        c.curs_set(0)
        return text

    # ==[ message bar ]========================================================
    def _update_status_msg(self, msg):
        try:
            self.screen.addstr(self.h-1, 0, self._get_long_line(msg))
        except: pass
        self._set_msgbar_timeout(1) # how many renders it will be on screen

    def _show_error_msg(self, msg):
        """ shows an error in the status bar. Need to set statusbar timeout
        to keep the error message visible for sometime """
        newmsg = "ERROR: %s" % msg
        try:
            self.screen.addstr(self.h-1, 0, self._get_long_line(newmsg),
                    c.color_pair(4))
        except: pass
        self._set_msgbar_timeout(1) # how many renders it will be on screen

    def _set_msgbar_timeout(self, timeout):
        """ self._dirty - render counter. if its 10, then it will trigger
        statusbar clean up after 10 renderings. """
        self._dirty = timeout

    def _chk_msgbar_dirty(self):
        """ self._dirty is render counter. if dirty is greater then 0, decrement
        everytime the function is run. If dirty equals 0, clean the statusbar
        and prevent further decrements (set to -1) """
        if self._dirty == 0:
            self.screen.hline(self.h-1, 0, " ", self.w)
            self._dirty -= 1
        if self._dirty > 0: self._dirty -= 1

    # ==[ statusbar ]==========================================================
    def _limit_length(self, msg, reserve):
        """ truncate msg to fit screen width - reserve """
        ln = self.w - reserve
        if len(msg) > ln:
            msg = "..." + msg[-(ln-3):]
        return msg

    def _create_statusbar_levels(self):
        """ create level hiararchy graph in statusbar """
        prfx = ">> "
        sfx = " "
        lmsg = ""
        if len(self._levbuf) > 0:
            for lev in self._levbuf:
                data, tpos, cur = lev
                lmsg += prfx + data[tpos+cur].name + sfx
        lmsg += prfx + self.data[self.dpos].name + sfx
        return self._limit_length(lmsg, 15)

    def _set_statusbar_bpercent(self, lines):
        """ calculate the percent of displayed content in the bottom window """
        self._bpercent = (float(self._bpos+self._blines) / len(lines)) * 100
        if self._bpercent > 100: self._bpercent = 100

    def _is_one_left(self):
        """ check if there is only one node left in the data structure """
        return len(self.data) ==  1 and len(self._levbuf) == 0

    def _create_statusbar(self):
        """ combine statusbar components together """
        lvl = self._create_statusbar_levels()
        filler = self.w - (len(lvl) + len(self.indicator))
        return "%s%s%s" % (lvl, ' '*filler, self.indicator)

    # ==[ render ]=============================================================
    def _render_top(self):
        """ render but not update the top window """
        self.topwin.erase()
        y = 0
        for n in self.data[self._tpos : self._tpos + self._tlines]:
            if (n.has_children()):
                prefix = "+"
            else:
                prefix = " "

            if (y == self._cursor):
                self.topwin.addstr(y, 0, self._get_long_line(prefix + n.name),
                        c.A_REVERSE)
            else:
                self.topwin.addstr(y, 0, prefix + n.name, c.color_pair(3))
            y += 1
        self.topwin.noutrefresh()

    def _render_bottom(self):
        """ render but not update the bottom windows """
        self.bottomwin.erase()
        y = 0
        try:
            lines = self._lines = self.data[self.dpos].value.split("\n")
            self._set_statusbar_bpercent(lines)
            for line in lines[self._bpos : self._bpos+self._blines-1]:
                try:
                    if line == "": line = "\n"
                    self.bottomwin.addstr(y, 0, str(line))
                except c.error: pass
                y += 1
        except AttributeError:
            pass
        self.bottomwin.noutrefresh()

    def _render(self):
        """ do all the rendering and update screen """
        title = "==[ CtrlV - Template manager ]=="
        self.h, self.w = self.screen.getmaxyx() # screem dymensions
        self.screen.addstr(0, 0, self._get_long_line(title, "="),
                c.color_pair(1) | c.A_BOLD)
        self.screen.hline(self._tlines+1, 0, "=", self.w, c.color_pair(3))
        self._render_top()
        self._render_bottom()
        self.screen.addstr(self.h-2, 0, self._create_statusbar(), c.color_pair(1))
        self._chk_msgbar_dirty()

    # ==[ global functions ]===================================================
    def cleanup(self):
        """ cleanup curses and restore normal mode """
        c.nocbreak(); self.screen.keypad(0); c.echo()
        c.endwin()

    def attach_controller(self, controller):
        """ attach controller - MVC pattern """
        self.__controller = controller
        self.data = controller.data

    # ==[ key bindings ]=======================================================
    def top_move_up(self, step=1):
        """ move cursor up "step" lines """
        self._bpos = 0 # setting the bootom scroll marker to very top
        if self._cursor < 1:
            if (self._tpos > 0):
                self._tpos -= step
        else:
            self._cursor -= step

    def top_move_down(self, step=1):
        """ move cursor down "step" lines """
        self._bpos = 0 # setting the bootom scroll marker to very top
        if self.dpos < len(self.data)-1:
            if self._cursor >= self._tlines-1:
                self._tpos += step
            else:
                self._cursor += step

    def bottom_move_up(self, step=10):
        """ scroll bottom window up "step" lines """
        if self._bpos > 0: self._bpos -= step

    def bottom_move_down(self, step=10):
        """ scroll bottom window down on "step" lines """
        lines = self._lines
        if (self._bpos + self._blines) < len(lines)+(self._blines / 3):
            self._bpos += step

    def level_down(self):
        """ this command pushes a chlidren list to the stack
        data, _tpos and cursor as storred in the levbuf as tupple (in that order),
        and then initializes the new context of the sublevel """
        if self.data[self.dpos].has_children():
            self._levbuf.append((self.data, self._tpos, self._cursor))
            self.data, self._tpos, self._cursor = self.data[self.dpos].children, 0, 0

    def level_up(self):
        """ this command pops upper level data back from the stack _levbuf
        data, tpos and cursor as storred in the levbuf as tupple (in that order) """
        if len(self._levbuf) > 0:
            self.data, self._tpos, self._cursor = self._levbuf.pop()

    def copy(self, signature=False):
        """ copy the data to X clipboard """
        selected = self.data[self.dpos]
        if (selected != None and selected.value != None):
            if not self.__controller.set_clipboard(selected.value, signature):
                self._show_error_msg("cannot access the clipboard. an error has occured")
            else:
                self._update_status_msg("clipboard updated")

    def copy_signature(self):
        if not self.__controller.set_clipboard("", True):
            self._show_error_msg("cannot access the clipboard. an error has occured")
        else:
            self._update_status_msg("clipboard updated")

    def edit(self):
        """ edit current node - temp file created and external editor is used """
        try:
            f = tempfile.NamedTemporaryFile()
            f.write(self.data[self.dpos].value)
            f.flush()
            os.system(self.__editor + " " + f.name)
            f.seek(0)
            data = f.read()
            if (data != None):
                self.data[self.dpos].value = data
            f.close()
        except:
            self._show_error_msg("Cannot spawn the editor")
        finally:
            # FIXME cursor is not hidden, although its set to 0
            self._setup_curses()
            self.screen.clear()
            self.screen.refresh()

    def insert(self, inside=False):
        """ insert a node under the cursor """
        name = self._get_user_input("enter a new name: ")
        if (name == ""):
            self._show_error_msg("name cannot be empty")
            return # do nothing if name is ""
        # TODO: need to add proper error handling here
        text = self.__controller.get_clipboard()
        node = ctrlv.io.CtrlvNode(name, text)
        if (inside):
            self.data[self.dpos].add_child(node)
            self.level_down()
        else:
            self.data.insert(self.dpos, node)

    def rename(self):
        """ rename current node """
        name = self._get_user_input("new name: ", self.data[self.dpos].name)
        if (name == ""):
            self._show_error_msg("name cannot be empty")
            return # do nothing if name is ""
        self.data[self.dpos].name = name

    def delete(self):
        """ delete a node under the cursor """
        if not self._is_one_left():
            self.data.pop(self.dpos)
            if len(self.data) == 0 and len(self._levbuf) > 0: self.level_up()
            if self.dpos > len(self.data)-1: self._cursor -= 1
        else:
            self._show_error_msg("there must be at least one node")

    def swap_up(self):
        """ swap to adjacent nodes, moving the current node up """
        if self.dpos > 0:
            self.data[self.dpos-1], self.data[self.dpos] = \
                    self.data[self.dpos], self.data[self.dpos-1]
            self.top_move_up()

    def swap_down(self):
        """ swap two adjacent nodes, moving the crrent node down """
        if self.dpos < len(self.data)-1:
            self.data[self.dpos+1], self.data[self.dpos] = \
                    self.data[self.dpos], self.data[self.dpos+1]
            self.top_move_down()

    def save(self):
        """ save data to disk by calling saver callback self.__saver must be
        registered by controller via register_saver function """
        if not self.__controller.write(self.root_data):
            self._show_error_msg("cannot write the data to disk. An error occured")
        else:
            self._update_status_msg("the file has been successfully saved")

    # ==[ main loop ]=========================================================
    def mainloop(self):
        """ main loop of the UI. rendering and event handling is done here """
        self.screen.refresh()
        try:
            while True:
                try:
                    self._render()
                    e = self.screen.getch()
                    if   e == ord("q"): break
                    elif e == ord("j") or e == c.KEY_DOWN : self.top_move_down()
                    elif e == ord("k") or e == c.KEY_UP   : self.top_move_up()
                    elif e == ord("p") or e == c.KEY_PPAGE: self.bottom_move_up()
                    elif e == ord("n") or e == c.KEY_NPAGE: self.bottom_move_down()
                    elif e == ord("l") or e == c.KEY_RIGHT: self.level_down()
                    elif e == ord("h") or e == c.KEY_LEFT : self.level_up()
                    elif e == ord("y") or e == ord(" ")   : self.copy()
                    elif e == ord("Y") or e == c.KEY_F4   : self.copy(signature=True)
                    elif e == ord("g") or e == c.KEY_F12  : self.copy_signature()
                    elif e == ord("e") or e == c.KEY_F6   : self.edit()
                    elif e == ord("i") or e == c.KEY_IC   : self.insert()
                    elif e == ord("I") or e == c.KEY_F5   : self.insert(inside=True)
                    elif e == ord("R") or e == c.KEY_F2   : self.rename()
                    elif e == ord("D") or e == c.KEY_DC   : self.delete()
                    elif e == ord(">") or e == c.KEY_SHOME: self.swap_up()
                    elif e == ord("<") or e == c.KEY_SEND : self.swap_down()
                    elif e == ord("S") or e == c.KEY_F10  : self.save()
                except KeyboardInterrupt:
                    pass
        finally:
            self.screen.refresh()
            self.cleanup()

class Controller(AbstractController):
    """ Controller class - responsible for linking UI with model """
    def __init__(self, conf, view):
        self.__conf = conf
        self.__data = None
        self.__signature = ""
        self.__xmlhandler = ctrlv.io.XmlHandler(conf["default_path"])
        if isinstance(view, View):
            self.__view = view
            self.__view.data = self.data
            self.__view.attach_controller(self) # registering self to the view

        # read signature and store it to __signature
        if os.path.exists(conf["signature"]):
            try:
                with open(conf["signature"], "r") as f:
                    self.__signature = f.read()
            except: pass

    @property
    def view(self):
        """ current UI implementation getter """
        return self.__view

    @property
    def signature(self):
        return self.__signature

    @property
    def data(self):
        """ getter for data. If not data not set, read from model """
        if (self.__data == None):
            self.__data = self.model.read()
        return self.__data

    @data.setter
    def data(self, data):
        self.__data = data

    def write(self, data):
        """ update models data, so that model writes it to disk """
        self.data = data
        self.model.data = self.data
        return self.model.write()

    @property
    def model(self):
        return self.__xmlhandler

    def get_clipboard(self):
        """ read clipboard """
        try:
            return os.popen(self.__conf["clip_read"]).read()
        except:
            return ""

    def set_clipboard(self, value, signature=False):
        """ write to clipboard """
        try:
            # if signature==True - append signature to the copied text
            if (signature):
                value += "\n" + self.__signature
            os.popen(self.__conf["clip_write"], "wb").write(value)
            return True
        except: return False

    def run(self):
        if self.view == None:
            print "ERROR: view is not set"
            del(self.view)
            return
        self.view.mainloop()

