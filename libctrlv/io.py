#!/usr/bin/env python2
#
# { io.py }
# Copyright (C) 2012 Alex Kozadaev [akozadaev at yahoo com]
#

import sys
import copy, xml.dom.minidom as xmldom

class CtrlvNode(object):
    """ Main CtrlV datastructure used to store the templates.
    Externally the contents is stored in XML format. """
    def __init__(self, name="", value=""):
        self.name = name
        self.value = value
        self.children = []

    def add_child(self, child):
        if not self.is_node(child):
            raise ValueError("Node instance expected")
        self.children.append(child)

    def has_children(self):
        return len(self.children) > 0

    def is_node(self, n):
        return isinstance(n, CtrlvNode)

class XmlHandler(object):
    def __init__(self, filename):
        self.__filename = filename
        self.data = None
        self.__doc = None

    def read(self):
        """ Parsing the xml document and populate data
        output: list of Node classes represending the xml doc"""
        self.data = []
        try:
            self.__doc = xmldom.parse(self.__filename);
        except IOError:
            self.data.append(CtrlvNode("New Node", ""))
            return self.data

        for xmlnode in self.__get_root_level(self.__doc):
            current = CtrlvNode(xmlnode.getAttribute("name"))
            self.__handle_childs(xmlnode, current)
            self.data.append(current)
        return self.data

    def write(self, fname=None):
        """ generate an XML document from the data and write it to disk """
        if (fname == None): fname = self.__filename

        doc = xmldom.Document()
        root = doc.createElement("CtrlV")
        doc.appendChild(root)
        self.__write_level(doc, root, self.data)

        try:
            with open(fname, "w+") as out:
                doc.writexml(out)
            return True
        except:
            return False

    def __handle_childs(self, xmlnode, parent):
        """ Parsing the Node "parent" and populate the data (no return)
        Working with 2 structures: Xml data and Node class data
        eg. xmlchild - xml tree child, child - Node class
        input: XML node and parent Node class """
        if xmlnode.hasChildNodes():
            for xmlchild in xmlnode.childNodes:
                if xmlchild.nodeName == "node":
                    child = CtrlvNode(xmlchild.getAttribute("name"))
                    self.__handle_childs(xmlchild, child)
                    parent.add_child(child)

                # If child.TEXT_NODE - body of the parent is also a child
                elif xmlchild.nodeType == xmlchild.TEXT_NODE:
                    parent.value = xmlchild.data.strip()

    def __get_root_level(self, doc):
        """ XML document root level generator
        Input: xml document
        output: xml nodes generator """
        nodes = doc.getElementsByTagName("node")
        for n in nodes:
            if (n.parentNode.nodeName == "CtrlV"):
                yield n

    def __write_level(self, doc, root, data):
        """ Writes current level of data (if a node has children - recursively
        write next levels too) """
        for node in data:
            xmlnode = doc.createElement("node")
            xmlnode.setAttribute("name", node.name)
            root.appendChild(xmlnode)
            if node.value != None and len(node.value) > 0:
                textnode = doc.createTextNode(node.value)
                xmlnode.appendChild(textnode)
            if node.has_children():
                self.__write_level(doc, xmlnode, node.children)

# vim: set tabstop=4 softtabstop=4 shiftwidth=4 smarttab expandtab
