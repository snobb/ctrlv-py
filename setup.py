#!/usr/bin/env python
#
# { setup.py }
# Copyright (C) 2013 Alex Kozadaev [akozadaev at yahoo com]
#

from distutils.core import setup
setup(name="ctrlv",
        description=(
            "ctrlv - a small python program for handling email "
            "templates for fast transfer to clipboard (xsel is used)."),
        author="Alex Kozadaev",
        author_email="akozadaev at yahoo com",
        license="MIT License",
        version="0.05b",
        scripts=["ctrlv"],
        py_modules=["libctrlv.io", "libctrlv.ui"],
        )


# vim: set ts=4 sts=4 sw=4 tw=80 ai smarttab et list
